# Установка
---
Мы рекомендуем использовать [Docker](https://www.docker.com/), т.к. он не требует установки Foliant и всех его  зависимостей. Если же вы хотите провести ручную установку, то её можно условно разделить на 3 стадии:

- Установка Foliant с помощью pip3 
- Установка дополнительных пакетов [Pandoc](https://pandoc.org/) и [TeXLive](http://www.tug.org/texlive/)
- Установка бэкендов

<pre style="background-color: LemonChiffon"><span>&#9888;</span><strong> ВНИМАНИЕ</strong>: Для успешной установки всех компонентов вы должны обладать правами  
администратора!</pre>


## Установка с помощью Docker

**1. Установка Docker**

Выберите вашу операционную систему и следуйте полученным инструкциям:

- [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
- [Windows](https://docs.docker.com/docker-for-windows/install/)
- [MacOS](https://docs.docker.com/docker-for-mac/install/)

>О других вариантах установки Вы можете прочитать [тут](https://docs.docker.com/). В меню навигации (слева) выберите: ***Get Docker >> Docker Engine-Community***.

**2. Установка контейнера Foliant**

    $ docker pull foliant/foliant

## Ubuntu

<pre style="background-color: LemonChiffon">Для ввода команд используйте терминал (Ctrl+Alt+T)</pre>

**1. Установка Python 3.6 и pip3**

- Для версий **14.04** и **16.04**:

        $ sudo add-apt-repository ppa:jonathonf/python-3.6
        $ sudo apt update && sudo apt install -y python3 python3-pip

- Для версий **18.04** и новее:

        $ sudo apt update && sudo apt install -y python3 python3-pip

**2. Установка Foliant через pip3**

    $ sudo python3.6 -m pip install foliant foliantcontrib.init

**3. Установка дополнительных пакетов Pandoc и TeXLive**

    $ sudo apt install -y wget texlive-full librsvg2-bin
    $ sudo wget https://github.com/jgm/pandoc/releases/download/2.0.5/pandoc-2.0.5-1-amd64.deb && sudo dpkg -i pandoc-2.0.5-1-amd64.deb

**4. Установка бэкендов**

- **Mkdocs** (для генерации web-сайта):

        $ sudo pip3 install foliantcontrib.mkdocs

- **Pandoc** (для генерации PDF, DOCX, TEX):

        $ sudo pip3 install foliantcontrib.pandoc

## Windows

<pre style="background-color: LemonChiffon"><span>&#9888;</span><strong> ВНИМАНИЕ</strong>: Для ввода команд используйте командную строку PowerShell</pre> 

**Требования**

+ Windows 64-bit (7 SP1, 8.1, Server 2008 R2 SP1, Server 2012, Server 2012 R2)
+ [PowerShell 5](https://www.microsoft.com/en-us/download/details.aspx?id=54616) или новее ( включая [PowerShell Core](https://docs.microsoft.com/en-us/powershell/scripting/install/installing-powershell-core-on-windows?view=powershell-6#installing-the-msi-package))
+ [.NET Framework 4.5](https://www.microsoft.com/ru-ru/search?q=.net+framework+4.5) или новее

**1. Установка Scoop через PowerShell**

    iex (new-object net.webclient).downloadstring('https://get.scoop.sh')

**2. Установка Python3 через Scoop**

    $ scoop install python

**3. Установка Foliant через pip**

    $ python -m pip install foliant foliantcontrib.init

**4. Установка Pandoc и MikTeX через Scoop**

    $ scoop install pandoc latex

## MacOS

<pre style="background-color: LemonChiffon">Для ввода команд используйте терминал (Command (⌘) + T)</pre>

**Требования**

- [Homebrew](https://brew.sh/)

**1. Установка Python 3 через Homebrew**

    $ brew install python3

**2. Установка Foliant через pip**

    $ python3 -m pip install foliant foliantcontrib.init

**3. Установка Pandoc и MacTeX через Homebrew**

    $ brew install pandoc mactex librsvg

